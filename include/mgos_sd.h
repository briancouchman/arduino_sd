/*
 * Copyright (c) 2014-2016 Cesanta Software Limited
 * All rights reserved
 */

#ifndef SD_ARDUINO_MGOS_ARDUINO_H_
#define SD_ARDUINO_MGOS_ARDUINO_H_

#include "mgos_features.h"
#include "mgos_init.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

bool mgos_arduino_sd_init(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SD_ARDUINO_MGOS_ARDUINO_H_ */
